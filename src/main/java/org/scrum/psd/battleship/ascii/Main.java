package org.scrum.psd.battleship.ascii;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.sound.sampled.Clip;

import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;

public class Main {
    private static List<Ship> myFleet;
    private static List<String> myFleetDown = new ArrayList<>();
    private static List<String> myFleetEnemeyDown = new ArrayList<>();
    private static List<Ship> enemyFleet;
    private static ColoredPrinter console;
    private static boolean winEnemy = false;
    private static boolean youWin = false;
    private static List<Position> shootedComputer = new ArrayList<Position>();

    public static void main(String[] args) {
    	
    	
        console = new ColoredPrinter.Builder(1, false).build();

        console.setForegroundColor(Ansi.FColor.WHITE);
        console.setBackgroundColor(Ansi.BColor.BLACK);
        console.println("                                     |__");
        console.println("                                     |\\/");
        console.println("                                     ---");
        console.println("                                     / | [");
        console.println("                              !      | |||");
        console.println("                            _/|     _/|-++'");
        console.println("                        +  +--|    |--|--|_ |-");
        console.println("                     { /|__|  |/\\__|  |--- |||__/");
        console.println("                    +---------------___[}-_===_.'____                 /\\");
        console.println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
        console.println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
        console.println("|                        Welcome to Battleship                         BB-61/");
        console.println(" \\_________________________________________________________________________|");
        console.println("");
        console.clear();

        InitializeGame();

        StartGame();
        
    }

    private static void StartGame() {
        Scanner scanner = new Scanner(System.in);

        console.setForegroundColor(Ansi.FColor.GREEN);
        console.setBackgroundColor(Ansi.BColor.BLACK);
        console.print("\033[2J\033[;H");
        console.println("                  __");
        console.println("                 /  \\");
        console.println("           .-.  |    |");
        console.println("   *    _.-'  \\  \\__/");
        console.println("    \\.-'       \\");
        console.println("   /          _/");
        console.println("  |      _  /\" \"");
        console.println("  |     /_\'");
        console.println("   \\    \\_/");
        console.println("    \" \"\" \"\" \"\" \"");

        do {
        	
        	console.println("----------");
//            console.println("SIZE YOU "+myFleet.size());
            console.println("My correct shots: "+myFleetEnemeyDown.size());
            console.println("---> My shots suscessfull position: ");
            for (String a: myFleetEnemeyDown) {
            	System.out.print(a+" ");
            }
            console.println("");
            console.println("----------");
            
//            console.println("SIZE ENEMY "+enemyFleet.size());
            console.println("Enemy correct shots: "+myFleetDown.size());
            console.println("---> Enemy shots suscessfull position:");
            for (String a: myFleetDown) {
            	System.out.print(a+" ");
            }
            console.println("");
            console.println("----------");
        	
        	console.setForegroundColor(Ansi.FColor.GREEN);
            console.setBackgroundColor(Ansi.BColor.BLACK);
            console.println("");
            console.println("Player, it's your turn");
            console.println("Enter coordinates for your shot :");
            Position position = parsePosition(scanner.next());
            boolean isHit = GameController.checkIsHit(enemyFleet, position);
            if (isHit) {
                beep();

                console.setForegroundColor(Ansi.FColor.RED);
//                console.setBackgroundColor(Ansi.BColor.RED);
                console.println("                \\         .  ./");
                console.println("              \\      .:\" \";'.:..\" \"   /");
                console.println("                  (M^^.^~~:.'\" \").");
                console.println("            -   (/  .    . . \\ \\)  -");
                console.println("               ((| :. ~ ^  :. .|))");
                console.println("            -   (\\- |  \\ /  |  /)  -");
                console.println("                 -\\  \\     /  /-");
                console.println("                   \\  \\   /  /");
                myFleetEnemeyDown.add(position.getColumn().toString()+position.getRow());
                if (myFleetEnemeyDown.size()==enemyFleet.size()){
                	youWin = true;
                	break;
                }
                
                console.println("Yeah ! Nice hit !");
                
                youWin(console);
//                System.exit(0);
                break;
                
            } else {
            	
            	console.setForegroundColor(Ansi.FColor.BLUE);
            	console.println("Miss");
            }

//            console.println(isHit ? "Yeah ! Nice hit !" : "Miss");
           

            console.setForegroundColor(Ansi.FColor.GREEN);
            console.setBackgroundColor(Ansi.BColor.BLACK);
//            console.println(".................................................");
            
            position = getValidShootPosition();
            //console.println("posi enem "+position.getColumn()+position.getRow());
            shootedComputer.add(position);
            isHit = GameController.checkIsHit(myFleet, position);
            console.setForegroundColor(Ansi.FColor.BLACK);
        	console.setBackgroundColor(Ansi.BColor.WHITE);
        	console.println("");
//            console.println(String.format("Computer shoot in %s%s and %s", position.getColumn(), position.getRow(), isHit ? "hit your ship !" : "miss"));
            if (isHit) {
                beep();

                console.setForegroundColor(Ansi.FColor.RED);
//                console.setBackgroundColor(Ansi.BColor.RED);
                console.println("                \\         .  ./");
                console.println("              \\      .:\" \";'.:..\" \"   /");
                console.println("                  (M^^.^~~:.'\" \").");
                console.println("            -   (/  .    . . \\ \\)  -");
                console.println("               ((| :. ~ ^  :. .|))");
                console.println("            -   (\\- |  \\ /  |  /)  -");
                console.println("                 -\\  \\     /  /-");
                console.println("                   \\  \\   /  /");
                myFleetDown.add(position.getColumn().toString()+position.getRow());
                if (myFleetDown.size()==myFleet.size()){
                	winEnemy = true;
                	break;
                }
                console.println(String.format("Computer shoot in %s%s and %s", position.getColumn(), position.getRow(), "hit your ship !"));
            } else {
            	console.setForegroundColor(Ansi.FColor.BLUE);
            	console.println("Miss");
            }
            console.setForegroundColor(Ansi.FColor.GREEN);
            console.setBackgroundColor(Ansi.BColor.BLACK);
            console.println(".................................................");
            
        } while (true);
        
     /*   if(youWin) {
        	console.println("YOUR WINNNNNNN!!!");
        }
        
        if(winEnemy) {
        	console.println("GAME OVER!!!");
        }*/
    }

    private static void beep() {
        console.print("\007");
    }

    protected static Position parsePosition(String input) {
        Letter letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
        int number = Integer.parseInt(input.substring(1));
        return new Position(letter, number);
    }

    private static Position getRandomPosition() {
        int rows = 8;
        int lines = 8;
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(lines)];
        int number = random.nextInt(rows);
        Position position = new Position(letter, number);
        return position;
    }
    
    private static Position getValidShootPosition() {
    	//boolean validFound = false;
    	do {
    		Position position = getRandomPosition();
    		boolean posFound = false;
            for (Position pos : shootedComputer) {
            	if (position.equals(pos)) {
            		posFound = true;
            		break;
            	}
            	
            }
            if (!posFound) return position;
    	} while (true);
        
    }
    
    private static void youWin(ColoredPrinter console)  {

		try {
			InputStream inputStream = ClassLoader.getSystemClassLoader().getResourceAsStream("youwin.txt");
			console.setForegroundColor(Ansi.FColor.YELLOW);
			console.println(readFromInputStream(inputStream));

		} catch (IOException e) {
		}
		
		
//    	Clip sound = GameController.getSound("0237.wav");
//    	GameController.playSound(sound);
//    	try {
//			Thread.sleep(1000*5);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
    	

    	
    	
    	
    }
    
    private static String readFromInputStream(InputStream inputStream)
    		  throws IOException {
    		    StringBuilder resultStringBuilder = new StringBuilder();
    		    try (BufferedReader br
    		      = new BufferedReader(new InputStreamReader(inputStream))) {
    		        String line;
    		        while ((line = br.readLine()) != null) {
    		            resultStringBuilder.append(line).append("\n");
    		        }
    		    }
    		  return resultStringBuilder.toString();
    		}

    private static void InitializeGame() {
        InitializeMyFleet();

        InitializeEnemyFleet();
    }

    private static void InitializeMyFleet() {

    	
    		Scanner scanner = new Scanner(System.in);
            myFleet = GameController.initializeShips();
            
            console.setForegroundColor(Ansi.FColor.WHITE);
            console.setBackgroundColor(Ansi.BColor.BLACK);

            console.println("Please position your fleet (Game board has size from A to H and 1 to 8) :");

            for (Ship ship : myFleet) {
                console.println("");
                console.println(String.format("Please enter the positions for the %s (size: %s)", ship.getName(), ship.getSize()));
                for (int i = 1; i <= ship.getSize(); i++) {
                	String positionInput = null;
                	boolean shipOk = false;
                	do {
                		console.println(String.format("Enter position %s of %s (i.e A3):", i, ship.getSize()));

                        positionInput = scanner.next();
                        
                        shipOk = GameController.validatePositionInput(positionInput, myFleet, 8, Letter.H);
                        if (!shipOk) {
                        	console.println("Invalid position!");
                        }
                        
                	} while (!shipOk);
                    
                    ship.addPosition(positionInput);
                }

            }
        
    }

    private static void InitializeEnemyFleet() {
        enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 5));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 7));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 8));

        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 7));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 8));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 9));

        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 3));

        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 8));

        enemyFleet.get(0).getPositions().add(new Position(Letter.C, 5));
        enemyFleet.get(0).getPositions().add(new Position(Letter.C, 6));
    }
}
