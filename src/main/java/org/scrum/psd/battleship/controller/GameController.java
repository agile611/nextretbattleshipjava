package org.scrum.psd.battleship.controller;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

import org.scrum.psd.battleship.controller.dto.Color;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

public class GameController {
    public static boolean checkIsHit(Collection<Ship> ships, Position shot) {
        if (ships == null) {
            throw new IllegalArgumentException("ships is null");
        }

        if (shot == null) {
            throw new IllegalArgumentException("shot is null");
        }

        for (Ship ship : ships) {
            for (Position position : ship.getPositions()) {
                if (position.equals(shot)) {
                	position.setTocado(true);
                    return true;
                }
            }
        }

        return false;
    }

    public static List<Ship> initializeShips() {
        return Arrays.asList(
                new Ship("Aircraft Carrier", 5, Color.CADET_BLUE),
                new Ship("Battleship", 4, Color.RED),
                new Ship("Submarine", 3, Color.CHARTREUSE),
                new Ship("Destroyer", 3, Color.YELLOW),
                new Ship("Patrol Boat", 2, Color.ORANGE));
    }

    public static boolean isShipValid(Ship ship) {
        return ship.getPositions().size() == ship.getSize();
    }

    public static Position getRandomPosition(int size) {
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(size)];
        int number = random.nextInt(size);
        Position position = new Position(letter, number);
        return position;
    }

	public static boolean validatePositionInput(String positionInput, List<Ship> myFleet, int num, Letter letter) {
		if (positionInput == null || positionInput.trim().isEmpty() || positionInput.length() != 2) return false;
		String[] positions = positionInput.split("");
		boolean letterFound = false;
		for (Letter letra : Letter.values()) {
			if (positions[0].toUpperCase().equals(String.valueOf(letra))) {
				letterFound = true;
				break;
			}
		}
		if (!letterFound) return false;
		int numero;
		try {
			numero = Integer.parseInt(positions[1]);
			if (numero < 1 || numero > num) return false;
		} catch (NumberFormatException e)  {
			return false;
		}
		for (Ship ship : myFleet) {
			for (Position pos : ship.getPositions()) {
				if (pos.getColumn().equals(positions[0]) && pos.getRow() == numero) return false;
			}
		}
		return true;
	}
	
	public static Clip getSound(String file)
	{
		try
		{
			InputStream inputStream = ClassLoader.getSystemClassLoader().getResourceAsStream(file);
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(inputStream);
			AudioFormat format = audioInputStream.getFormat();
			DataLine.Info info = new DataLine.Info(Clip.class, format);
			Clip sound = (Clip)AudioSystem.getLine(info);
			sound.open(audioInputStream);
			return sound;
		}
		catch(Exception e)
		{
			return null;
		}
	}
 
	public static void playSound(Clip clip)
	{
		if (clip != null) {
			clip.stop();
			clip.setFramePosition(0);
			clip.start();
		}
		
	}
}
